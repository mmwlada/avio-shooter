﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AvioShooter
{
    /// <summary>
    /// An enumeration of the projectile types
    /// </summary>
    public enum ProjectileType
    {
        FrenchFries,
        TeddyBear
    }
}
